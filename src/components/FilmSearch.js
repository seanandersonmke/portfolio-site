import React from "react";
import DataList from "./filmSearch/DataList";
import SingleFilm from "./filmSearch/SingleFilm";
import * as loader from "../img/iLoading.gif";

class FilmSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentView: "init",
      loaderState: "",
      searchTerm: "",
      filmData: ""
    };
    this.input = React.createRef();
    this.getFilmConfig().then(response => {
      this.apiConfig = response;
    });
  }

  getFilmConfig = () => {
    const requestPath = `https://api.themoviedb.org/3/configuration?api_key=${
      process.env.REACT_APP_FILMKEY
    }`;
    return fetch(requestPath)
      .then(res => res.json())
      .then(
        result => {
          return result;
        },
        error => {
          console.log(error);
        }
      );
  };

  componentWillReceiveProps(props) {
    switch (props.setFilmView) {
      case "list":
        this.setState({
          currentView: "list",
          listAnimation: "filmSearch__dataList--show"
        });
        break;
      case "init":
        this.setState({ currentView: "init", listAnimation: "" });
        break;
      default:
        this.setState({ currentView: "init", listAnimation: "" });
    }
  }

  handleSubmit = event => {
    event.preventDefault();
    let value = this.input.current.value;
    if (!value) {
      return;
    }

    window.ga("send", "event", {
      eventCategory: "Film Search",
      eventAction: "submit",
      eventLabel: value
    });

    this.setState({
      loaderState: "filmSearch__loader--show",
      searchTerm: value,
      currentView: "list"
    });

    this.props.initView("list");
    value = "";
  };

  showSingleFilm = film => {
    this.setState({ filmData: film, currentView: "single" });
  };

  render() {
    return (
      <div className="filmSearch">
        <img
          className={"filmSearch__loader " + this.state.loaderState}
          src={loader}
          alt="Loader"
        />
        <form className="filmSearch__form" onSubmit={this.handleSubmit}>
          <input
            className="filmSearch__title"
            type="text"
            ref={this.input}
            aria-label="Enter Film Title"
          />
          <input className="filmSearch__submit" type="submit" value="Search" />
        </form>
        <DataList
          currentView={this.state.currentView}
          searchTerm={this.state.searchTerm}
          listAnimation={this.state.listAnimation}
          showSingleFilm={this.showSingleFilm}
          apiConfig={this.apiConfig}
        />
        <SingleFilm
          filmData={this.state.filmData}
          apiConfig={this.apiConfig}
          currentView={this.state.currentView}
        />
      </div>
    );
  }
}

export default FilmSearch;
