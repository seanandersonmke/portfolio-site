import React from "react";
import HackTellImg from "../img/hackntell.jpg";
import Synths from "../img/mysynths.jpg";

class BrowserSynth extends React.Component {
  googleAnalytics = event => {
    window.ga("send", "event", {
      eventCategory: "Browser Synth",
      eventAction: "click",
      eventLabel: event
    });
  };

  render() {
    return (
      <div className="browserSynth">
        <img
          src={Synths}
          alt="My synthesizers"
          className="browserSynth__mySynths"
        />
        <div className="browsersynth__inner">
          <img
            className="browserSynth__hacktellImg"
            src={HackTellImg}
            alt="Hack 'n Tell"
          />
          <h2>Music Tech</h2>
          <p>
            Music technology is a big hobby of mine. I am obsessed with
            synthesizers. This year I particapted in my first Hack &apos;n Tell
            here in Milwaukee. I decided to spend my 7 hours building a browser
            synthesizer using the Web Audio API. My first Hack &apos;n Tell and
            I won 2nd place. Couldn&apos;t even believe it!
          </p>
          <p>
            I have dabbled with the Web Audio and Midi APIs. This is the result
            of my experiment:{" "}
            <a
              onClick={() => this.googleAnalytics("browsersynth site")}
              href="http://browsersynth.net/"
            >
              http://browsersynth.net/
            </a>
          </p>
          <p>
            Source:{" "}
            <a
              onClick={() => this.googleAnalytics("browsersynth source")}
              href="https://bitbucket.org/seanandersonmke/browsersynth/src"
            >
              Bitbucket
            </a>
          </p>
          <h3>DISCLAIMER!</h3>
          <p>
            For best results (and possibly any results), use Chrome Desktop
            browser. I know this is forbidden for web devs to say, but due to
            the experimental nature of this technology, it is not very cross
            device compatible.
          </p>
          <p>
            For example,{" "}
            <a href="https://developer.mozilla.org/en-US/docs/Web/API/AudioNode/connect">
              <code>AudioNode.connect()</code>
            </a>
            , which I use a lot in this, returns <code>undefined</code> in
            Safari.
          </p>
          <q>
            If the destination is a node, <code>connect()</code> returns a
            reference to the destination AudioNode object, allowing you to chain
            multiple <code>connect()</code> calls. In some browsers, older
            implementations of this interface return undefined.
          </q>
          <p>So unfortunately, that alone takes out every Apple device.</p>
          <p>
            But nevertheless, it was a fun experiment, and this is the exact
            result from the end of the Hack &apos;n Tell.
          </p>
          <div style={{ clear: "both" }} />
        </div>
      </div>
    );
  }
}

export default BrowserSynth;
