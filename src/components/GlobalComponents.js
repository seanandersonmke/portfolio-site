import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SpaceStuff from "./SpaceStuff";
import BrowserSynth from "./BrowserSynth";
import "./FontLibrary";
import * as Me from "../img/sean.jpg";

class SinglePage extends React.Component {
  constructor(props) {
    super(props);
    let content;
    const { id, icon, title, subtitle, customContent } = this.props;

    switch (customContent) {
      case "aboutme":
        content = <AboutMe />;
        break;
      case "spacestuff":
        content = <SpaceStuff />;
        break;
      case "browsersynth":
        content = <BrowserSynth />;
        break;
      default:
        content = null;
    }
    this.data = {
      id: id ? id : "",
      icon: icon ? <FontAwesomeIcon icon={icon} /> : null,
      title: title ? <h1 className="page__title">{title}</h1> : null,
      subtitle: subtitle ? (
        <h2 className="page__subtitle">{subtitle}</h2>
      ) : null,
      content: customContent ? content : null
    };
  }

  render() {
    const { title, subtitle, content } = this.data;
    return (
      <section className={this.props.className} id={this.data.id}>
        <div className="page__container">
          <div className="page__iconContainer">
            <div className="page__icon">{this.data.icon}</div>
            {title}
            {subtitle}
          </div>
          {content}
        </div>
        {this.props.children}
      </section>
    );
  }
}

class AboutMe extends React.Component {
  googleAnalytics = event => {
    window.ga("send", "event", {
      eventCategory: "Portfolio source code",
      eventAction: "click",
      eventLabel: event
    });
  };

  render() {
    return (
      <div className="aboutMe">
        <div className="aboutMe__listContainer">
          <img src={Me} alt="Sean Anderson" className="aboutMe__profileImg" />
          <div className="aboutMe__list">
            <div className="aboutMe__listItem">
              <span>Name: </span>Sean Anderson
              <div className="aboutMe__pencilLine aboutMe__pencilLine--one" />
            </div>
            <div className="aboutMe__listItem">
              <span>Location: </span>Milwaukee, WI
              <div className="aboutMe__pencilLine aboutMe__pencilLine--two" />
            </div>
            <div className="aboutMe__listItem">
              <span>Email: </span>
              <a href="mailto:seanandersonmke@gmail.com">
                seanandersonmke@gmail.com
              </a>
              <div className="aboutMe__pencilLine aboutMe__pencilLine--three" />
            </div>
            <div className="aboutMe__listItem">
              <span>Source Code: </span>
              <a
                onClick={() =>
                  this.googleAnalytics("portfolio site source code")
                }
                href="https://bitbucket.org/seanandersonmke/portfolio-site/src/master/"
              >
                Bitbucket
              </a>
              <div className="aboutMe__pencilLine aboutMe__pencilLine--four" />
            </div>
          </div>
        </div>
        <div className="aboutMe__textContainer">
          <div className="aboutMe__textBorder">
            <p>
              My name is Sean Anderson. I am a web developer based in Milwaukee,
              WI with a specialization in front end technologies. I made this
              website to show off my web development abilities. I have used the
              React.js library. I connect to several different public APIs to
              display data with various user interactions. It is hosted in a
              Amazon Web Services S3 bucket behind Cloudfront and I use AWS CLI
              to deploy the site. I have experience building design heavy
              marketing websites as well as data heavy business facing web
              applications. I am currently available for a full time position or
              short/longer term contract work. I am available to work onsite in
              Milwaukee or remotely elsewhere.
            </p>
          </div>
        </div>
        <div className="aboutMe__skillsContainer">
          <h2 className="aboutMe__skillsLeadin">Specializing in:</h2>
          <SkillsMeter skillTitle="HTML" percent="94%" />
          <SkillsMeter skillTitle="CSS (SCSS/LESS)" percent="92%" />
          <SkillsMeter
            skillTitle="Javascript (Vanilla, Frameworks, Libraries, ES6)"
            percent="82%"
          />
          <SkillsMeter skillTitle="SEO" percent="90%" />
          <SkillsMeter skillTitle="Accessibility Optimization" percent="88%" />
          <SkillsMeter
            skillTitle="Front End Build Tools (Gulp, Grunt, Webpack, NPM, Node)"
            percent="76%"
          />
          <SkillsMeter
            skillTitle="PHP (Wordpress sites are easy. Some exp Laravel/CodeIgniter)"
            percent="76%"
          />
          <SkillsMeter
            skillTitle="Some experience with server admin, Apache, Tomcat, Jackrabbit, JCR, various Amazon Web Services, etc"
            percent="65%"
          />
        </div>
      </div>
    );
  }
}

class SkillsMeter extends React.Component {
  render() {
    const { skillTitle, percent } = this.props;
    return (
      <div className="aboutMe__skillItem">
        <p className="aboutMe__skillTitle">{skillTitle}</p>
        <div className="aboutMe__meterContainer">
          <div className="meter">
            <span style={{ width: percent }} />
          </div>
          <span>{percent}</span>
        </div>
      </div>
    );
  }
}

export default SinglePage;
