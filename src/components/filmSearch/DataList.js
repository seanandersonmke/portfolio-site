import React from "react";
import Masonry from "react-masonry-component";

class DataList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      html: [],
      searchTerm: "",
      viewMoreState: "filmSearch__viewMoreBtn--show",
      listAnimation: "",
      totalPages: "",
      noResults: ""
    };
  }

  showSingleFilm(item) {
    this.props.showSingleFilm(item);

    window.ga("send", "event", {
      eventCategory: "Show Single Film",
      eventAction: "click",
      eventLabel: item.original_title
    });
  }

  viewMore = () => {
    let html = [],
      results;
    const { currentView, searchTerm } = this.props,
      requestPath = `https://api.themoviedb.org/3/search/movie?query=${encodeURI(
        searchTerm
      )}&api_key=${process.env.REACT_APP_FILMKEY}&page=${this.state.page}`;

    fetch(requestPath)
      .then(res => res.json())
      .then(
        result => {
          if (result.status_code === 25) {
            return;
          }

          results = result.results;

          if (results.length === 0) {
            this.setState({
              noResults: (
                <h2 className="filmSearch__noResults">No results found</h2>
              ),
              viewMoreState: ""
            });
          } else {
            switch (currentView) {
              case "init":
                this.setState({ listAnimation: "", noResults: "" });
                break;
              case "list":
                var state = {
                  listAnimation: "filmSearch__dataList--show",
                  totalPages: result.total_pages,
                  page: result.page,
                  noResults: ""
                };

                if (result.total_pages === result.page) {
                  state.viewMoreState = "";
                } else {
                  state.viewMoreState = "filmSearch__viewMoreBtn--show";
                }
                this.setState(state);
                break;
              default:
                this.setState({ listAnimation: "", noResults: "" });
            }

            for (var index = 0; index < results.length; index++) {
              let baseUrl = this.props.apiConfig.images.base_url,
                posterSize = this.props.apiConfig.images.poster_sizes[4],
                posterPath = results[index].poster_path;
              if (
                baseUrl !== null &&
                posterSize !== null &&
                posterPath !== null
              ) {
                var imgSource = baseUrl + posterSize + posterPath;
                html.push({
                  imgSrc: imgSource,
                  id: results[index].id,
                  item: results[index]
                });
              }
            }
            if (this.state.page === 1) {
              this.setState({
                html: html
              });
            } else {
              this.setState({
                html: [...this.state.html, ...html]
              });
            }
          }
        },
        error => {
          console.log(error);
        }
      );
  };

  counter = props => {
    this.setState({ page: this.state.page + 1 }, () => this.viewMore());
  };

  componentWillReceiveProps(props) {
    if (props.searchTerm !== this.state.searchTerm) {
      this.setState({ searchTerm: props.searchTerm, page: 1, html: [] }, () =>
        this.viewMore()
      );
    }
  }

  render() {
    return (
      <div className={"filmSearch__dataList " + this.props.listAnimation}>
        {this.state.noResults}
        <Masonry
          options={{
            columnWidth: 20,
            itemSelector: ".filmSearch__img",
            percentPosition: true
          }}
        >
          {this.state.html.map(item => (
            <img
              alt={item.name}
              key={item.id}
              className="filmSearch__img"
              onClick={() => this.showSingleFilm(item.item)}
              src={item.imgSrc}
            />
          ))}
        </Masonry>
        <button
          className={"filmSearch__viewMoreBtn " + this.state.viewMoreState}
          onClick={() => this.counter(this.props)}
        >
          View <br /> More
        </button>
      </div>
    );
  }
}

export default DataList;
