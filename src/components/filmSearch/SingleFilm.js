import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../FontLibrary";

class SingleFilm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      singleFilmState: ""
    };

    this.filmConfig = {};
  }

  componentWillReceiveProps(props) {
    const { currentView, filmData } = props;
    if (currentView === "init" || currentView === "list") {
      this.setState({ singleFilmState: "" });
    } else if (filmData !== "") {
      this.setState({ singleFilmState: "filmSearch__singleFilm--show" });
    }
  }

  buildList = () => {
    let html = [];

    if (this.props.filmData !== "") {
      const {
        original_title,
        overview,
        release_date,
        vote_average,
        original_language
      } = this.props.filmData;

      if (original_title !== null) {
        html.push(
          <li key={original_title}>
            <span className="filmSearch__statName">Original Title: </span>
            <span className="filmSearch__statData">{original_title}</span>
          </li>
        );
      }

      if (release_date !== null) {
        html.push(
          <li key={release_date}>
            <span className="filmSearch__statName">Release Date: </span>
            <span className="filmSearch__statData">{release_date}</span>
          </li>
        );
      }

      if (original_language !== null) {
        html.push(
          <li key={original_language}>
            <span className="filmSearch__statName">Original Language: </span>
            <span
              className="filmSearch__statData"
              style={{ textTransform: "uppercase" }}
            >
              {original_language}
            </span>
          </li>
        );
      }

      if (overview !== null) {
        html.push(
          <li key={overview}>
            <span className="filmSearch__statName">Overview: </span>
            <span className="filmSearch__statData">{overview}</span>
          </li>
        );
      }

      if (vote_average !== null) {
        html.push(
          <li key={vote_average}>
            <span className="filmSearch__statName">Vote Average: </span>
            <span className="filmSearch__statData">{vote_average}</span>
          </li>
        );
      }

      const listItems = html.map(item => item);

      return listItems;
    }
  };

  getBackgroundImg = () => {
    if (this.props.filmData && this.props.filmData !== "") {
      let html = [];
      this.filmConfig = {
        baseUrl: this.props.apiConfig.images.base_url,
        backdropSize: this.props.apiConfig.images.backdrop_sizes[2],
        backdropPath: this.props.filmData.backdrop_path
      };

      const { baseUrl, backdropSize, backdropPath } = this.filmConfig;

      if (backdropPath) {
        html.push(
          <img
            alt={this.props.filmData}
            key={backdropPath}
            src={baseUrl + backdropSize + backdropPath}
            className="filmSearch__backdropImg"
          />
        );

        return html;
      } else {
        return "";
      }
    }
  };

  getPosterImage = () => {
    if (this.props.filmData !== "") {
      let html = [];
      const { apiConfig, filmData } = this.props;
      const { baseUrl, posterSize, posterPath } = {
        baseUrl: apiConfig.images.base_url,
        posterSize: apiConfig.images.poster_sizes[4],
        posterPath: filmData.poster_path
      };

      if (posterPath !== null || posterPath !== "") {
        html.push(
          <img
            key={posterPath}
            alt={filmData.original_title + " poster"}
            src={baseUrl + posterSize + posterPath}
          />
        );
        return html;
      }

      return "";
    }
  };

  hideFilm = () => {
    this.setState({ singleFilmState: "" });
  };

  render() {
    if (this.props.currentView)
      return (
        <div className={"filmSearch__singleFilm " + this.state.singleFilmState}>
          <div className="filmSearch__innerContainer">
            {this.getBackgroundImg()}
            <ul className="filmSearch__singleData">{this.buildList()}</ul>
          </div>
          <div className="filmSearch__posterContainer">
            {this.getPosterImage()}
          </div>
          <button className="filmSearch__backBtn">
            <FontAwesomeIcon
              icon="arrow-alt-circle-left"
              size="lg"
              onClick={this.hideFilm}
            />
          </button>
        </div>
      );
  }
}

export default SingleFilm;
