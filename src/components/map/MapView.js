import React from "react";

class MapView extends React.Component {
  render() {
    const { map, mapPosition } = this.props;
    if (map) {
      map.setCenter(mapPosition);
    }

    let view;
    switch (this.props.viewState) {
      case "map":
        view = "customMap__mapContainer--show";
        break;
      case "mapDetailView":
        view = "customMap__mapContainer--show";
        break;
      case "init":
        view = "";
        break;
      default:
        view = "";
    }
    return (
      <div className={"customMap__mapContainer " + view}>
        <div className="customMap__map" id="customMap" />
        <input type="text" className="customMap__search" />
      </div>
    );
  }
}

export default MapView;
