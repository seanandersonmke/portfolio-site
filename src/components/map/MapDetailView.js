import React from "react";
import ImageModal from "./ImageModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../FontLibrary";

class MapDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imgUrl: ""
    };
  }

  buildDetailView = () => {
    const {
      name,
      formatted_address,
      formatted_phone_number,
      website
    } = this.props.placeData;
    let domArray = [];

    if (name) {
      domArray.push(
        <h2 key={name} className="mapDetailView__title">
          {name}
        </h2>
      );
    }

    if (formatted_address) {
      domArray.push(
        <div key={formatted_address} className="mapDetailView__listItem">
          <div className="mapDetailView__addressContainer">
            <span>Address: </span>
            <span className="address">{formatted_address}</span>
          </div>
          <div className="mapDetailView__pencilLine mapDetailView__pencilLine--one" />
        </div>
      );
    }

    if (formatted_phone_number) {
      domArray.push(
        <div key={formatted_phone_number} className="mapDetailView__listItem">
          <span>Phone: </span>
          {formatted_phone_number}
          <div className="mapDetailView__pencilLine mapDetailView__pencilLine--two" />
        </div>
      );
    }

    if (website) {
      domArray.push(
        <div key={website} className="mapDetailView__listItem">
          <span>Website: </span>
          <a href={website}>Link</a>
        </div>
      );
    }

    return domArray;
  };

  getPlaceImages = () => {
    let images = [],
      imgUrl,
      viewport = this.getViewportSize();

    const { photos } = this.props.placeData;

    if (photos) {
      photos.forEach(photo => {
        imgUrl = photo.getUrl({ maxHeight: viewport });
        images.push(
          <img
            alt=""
            key={imgUrl}
            className="mapDetailView__img"
            src={imgUrl}
            onClick={() => this.showImageModal(photo)}
          />
        );
      });
    }

    return images;
  };

  getViewportSize = () => {
    const width = window.innerWidth;

    if (width <= 450) {
      return 100;
    } else if (width >= 451) {
      return 200;
    }

    return 100;
  };

  isOdd = num => {
    if (num % 2) {
      return "mapDetailView__divider--odd";
    }

    if (num % 3) {
      return "mapDetailView__divider--even";
    }

    return "";
  };

  buildReviewsView = () => {
    let html,
      count = 0;
    const { reviews } = this.props.placeData;

    if (reviews) {
      html = (
        <div className="mapDetailView__reviews">
          <div className="mapDetailView__list">
            <h2 className="mapDetailView__title">Reviews</h2>
            <div className="mapDetailView__leadinDivider" />
            {reviews.map(item => (
              <div className="mapDetailView__reviewItem">
                <div className="mapDetailView__reviewItemFlex">
                  <img
                    key={item.time}
                    className="mapDetailView__reviewIcon"
                    src={item.profile_photo_url}
                    alt={item.author_name + " icon"}
                  />
                  <h5 key={item.author_name}>{item.author_name}</h5>
                </div>
                <div
                  className={
                    "mapDetailView__stars mapDetailView__stars--" + item.rating
                  }
                />
                <p>{item.text}</p>
                <div
                  className={"mapDetailView__divider " + this.isOdd(count++)}
                />
              </div>
            ))}
          </div>
        </div>
      );
    }

    return html;
  };

  showImageModal = photo => {
    let url = photo.getUrl({ maxHeight: 600 });
    this.setState({ imgUrl: url });
  };

  removeSrc = () => {
    this.setState({ imgUrl: "" });
  };

  render() {
    let view;
    switch (this.props.viewState) {
      case "mapDetailView":
        view = "mapDetailView--show";
        break;
      default:
        view = "";
    }

    if (this.props.placeData) {
      return (
        <div className={"mapDetailView  " + view}>
          <button className="mapDetailView__backBtn">
            <FontAwesomeIcon
              icon="arrow-alt-circle-left"
              size="lg"
              onClick={() => this.props.setViewState("map")}
            />
          </button>
          <div className="mapDetailView__flex">
            <div className="mapDetailView__photos">
              <div className="mapDetailView__list">
                {this.buildDetailView()}
              </div>
              {this.getPlaceImages()}
            </div>
            {this.buildReviewsView()}
          </div>
          <ImageModal
            imgUrl={this.state.imgUrl}
            initModal={this.state.initModal}
            removeSrc={this.removeSrc}
          />
        </div>
      );
    } else {
      return <div className={"mapDetailView"} />;
    }
  }
}

export default MapDetailView;
