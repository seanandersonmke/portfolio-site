import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../FontLibrary";

class ImageModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleModal: ""
    };
  }

  hideModal = () => {
    this.setState({ toggleModal: "" }, function() {
      this.props.removeSrc();
    });
  };

  componentWillReceiveProps(props) {
    if (props.imgUrl !== "") {
      this.setState({ toggleModal: "imageModal--show" });
    }
  }

  render() {
    if (this.props.imgUrl !== "") {
      return (
        <div className={"imageModal " + this.state.toggleModal}>
          <button className="imageModal__backBtn">
            <FontAwesomeIcon
              icon="arrow-alt-circle-left"
              size="lg"
              onClick={this.hideModal}
            />
          </button>
          <img alt="" className="imageModal__img" src={this.props.imgUrl} />
        </div>
      );
    } else {
      return <div className={"imageModal " + this.state.toggleModal} />;
    }
  }
}

export default ImageModal;
