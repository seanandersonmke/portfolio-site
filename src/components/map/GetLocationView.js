import React from "react";

class GetLocationView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noResults: "hidden"
    };
    this.input = React.createRef();
  }

  handleGetLocationSubmit = event => {
    event.preventDefault();
    if (!this.input.current.value) {
      return;
    }

    const request = {
        address: this.input.current.value
      },
      geocoder = new window.google.maps.Geocoder(),
      self = this;
    geocoder.geocode(request, callback);

    function callback(results, status) {
      console.log(results);
      if (results.length) {
        let pos = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        self.props.setViewState("map");
        self.props.setPosition(pos);
        self.setState({ noResults: "hidden" });
      } else {
        self.setState({ noResults: "" });
      }
    }
  };

  render() {
    let view;
    switch (this.props.viewState) {
      case "getLocation":
        view = "customMap__mapContainer--show";
        break;
      default:
        view = "";
    }

    return (
      <div className={"customMap__getLocation " + view}>
        <form onSubmit={this.handleGetLocationSubmit}>
          <label className="customMap__label">Enter a City</label>
          <div className="customMap__inputContainer">
            <input
              ref={this.input}
              type="text"
              name="locationValue"
              className="customMap__input"
            />
            <input type="submit" value="Submit" className="customMap__submit" />
          </div>
          <div className={"customMap__noResults " + this.state.noResults}>
            No Results Found.
          </div>
        </form>
      </div>
    );
  }
}

export default GetLocationView;
