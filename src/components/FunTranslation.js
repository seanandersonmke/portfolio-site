import React from "react";

class FunTranslation extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
    this.state = {
      yodaText: ""
    };
  }

  handleSubmit = event => {
    event.preventDefault();
    if (!this.input.current.value) {
      return;
    }

    const requestPath = `http://www.translatefun.com/pidginenglish?q=${encodeURI(
      this.input.current.value
    )}`;
    fetch(requestPath)
      .then(res => res.json())
      .then(result => {
        console.log(result);
        this.setState({ yodaText: result });
      });
  };

  render() {
    return (
      <div className="funTranslation">
        <form onSubmit={this.handleSubmit}>
          <div className="funTranslation__inputContainer">
            <textarea
              className="funTranslation__input"
              type="text"
              ref={this.input}
              aria-label="Enter text to translate"
            />
            <input className="funTranslation__submit" type="submit" />
          </div>
        </form>
        <div className="funTranslation__textContainer">
          {this.state.yodaText}
        </div>
      </div>
    );
  }
}

export default FunTranslation;
