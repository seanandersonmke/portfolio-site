import React from "react";

class SpaceStuff extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      html: []
    };
  }

  componentDidMount() {
    const request = `https://api.nasa.gov/planetary/apod?api_key=${
      process.env.REACT_APP_NASAKEY
    }`;
    return fetch(request)
      .then(res => res.json())
      .then(result => {
        let data = {
          date: result.date ? result.date : "",
          explanation: result.explanation ? <p>{result.explanation}</p> : null,
          title: result.title ? <h3>{result.title}</h3> : null,
          img: result.url ? (
            <img src={result.url} alt="NASA pic of the day" />
          ) : null
        };

        this.setState({ html: data });
      });
  }

  render() {
    const { title, explanation, img } = this.state.html;
    return (
      <div className="nasa">
        <div className="nasa__inner">
          {title}
          {explanation}
          {img}
        </div>
      </div>
    );
  }
}

export default SpaceStuff;
