import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./FontLibrary";

class MainNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleIcon: " hidden"
    };
  }

  componentWillReceiveProps(props) {
    switch (props.setFilmView) {
      case "init":
        this.setState({ toggleIcon: "" });
        break;
      case "list" || "single":
        this.setState({ toggleIcon: "mainNav__icon--show" });
        break;
      default:
        this.setState({ toggleIcon: "" });
    }
  }

  render() {
    return (
      <nav className="mainNav" id="menu">
        <div className="mainNav__main">
          <a
            href="#homepage"
            className="mainNav__icon"
            onClick={e => this.props.initView("init", "homepage")}
            id="homePage"
          >
            <FontAwesomeIcon icon="user" />
          </a>
          <a
            href="#filmsearch"
            className="mainNav__icon"
            onClick={e => this.props.initView("init", "films")}
            id="pageOne"
          >
            <FontAwesomeIcon icon="film" />
          </a>
          <a
            href="#mapdemo"
            className="mainNav__icon"
            onClick={e => this.props.initView("init", "maps")}
            id="pageTwo"
          >
            <FontAwesomeIcon icon="map" />
          </a>
          <a
            href="#browsersynth"
            className="mainNav__icon"
            onClick={e => this.props.initView("init", "browsersynth")}
            id="pageThree"
          >
            <FontAwesomeIcon icon="music" />
          </a>
          <a
            href="#spacestuff"
            className="mainNav__icon"
            onClick={e => this.props.initView("init", "space")}
            id="pageFour"
          >
            <FontAwesomeIcon icon="space-shuttle" />
          </a>
        </div>
        <div className="mainNav__sub">
          <a
            href="#filmsearch"
            className={"mainNav__icon " + this.state.toggleIcon}
            onClick={e => this.props.initView("init", "search")}
          >
            <FontAwesomeIcon icon="search" />
          </a>
        </div>
      </nav>
    );
  }
}

export default MainNav;
