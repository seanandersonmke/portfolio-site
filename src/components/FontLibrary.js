import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faFilm,
  faMap,
  faSearch,
  faArrowAltCircleLeft,
  faUser,
  faSpaceShuttle,
  faMusic
} from "@fortawesome/free-solid-svg-icons";

library.add([
  faFilm,
  faMap,
  faSearch,
  faArrowAltCircleLeft,
  faUser,
  faSpaceShuttle,
  faMusic
]);
