import React from "react";
import GetLocationView from "./map/GetLocationView";
import MapView from "./map/MapView";
import MapDetailView from "./map/MapDetailView";
import MapStyles from "./MapStyles";

class CustomMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewState: "init",
      map: null,
      pos: {
        lat: 74.006,
        lng: 40.7128
      },
      noResults: "hidden",
      showDetails: "",
      placeData: ""
    };
  }

  componentDidMount() {
    if (!window.google) {
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.src = `https://maps.googleapis.com/maps/api/js?key=${
        process.env.REACT_APP_GMAPKEY
      }&libraries=places`;
      var x = document.getElementsByTagName("script")[0];
      x.parentNode.insertBefore(s, x);
      //We cannot access google.maps until it's finished loading
      s.addEventListener("load", e => {
        this.onScriptLoad();
      });
    } else {
      this.onScriptLoad();
    }
  }

  onScriptLoad = () => {
    //create map objects
    let searchInput = document.querySelector(".customMap__search"),
      searchBox = new window.google.maps.places.SearchBox(searchInput),
      bounds = new window.google.maps.LatLngBounds(),
      self = this,
      markers = [],
      map = new window.google.maps.Map(document.getElementById("customMap"), {
        center: this.state.prop,
        zoom: 13,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        styles: MapStyles
      });
    //add controls to map
    map.controls[window.google.maps.ControlPosition.TOP_RIGHT].push(
      searchInput
    );
    //add places services to map
    let service = new window.google.maps.places.PlacesService(map);
    //Add event listeners
    //keep searchbox within bounds
    map.addListener("bounds_changed", function() {
      searchBox.setBounds(map.getBounds());
    });
    //get new data when submitting new search
    searchBox.addListener("places_changed", function() {
      let places = searchBox.getPlaces();

      if (places.length === 0) {
        return;
      }
      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });

      markers = [];
      // For each place, get the icon, name and location.
      places.forEach(function(place) {
        if (!place.geometry) {
          return;
        }
        //set icon config
        let icon = {
          url: place.icon,
          size: new window.google.maps.Size(71, 71),
          origin: new window.google.maps.Point(0, 0),
          anchor: new window.google.maps.Point(17, 34),
          scaledSize: new window.google.maps.Size(25, 25)
        };
        //get details of clicked place
        service.getDetails(
          {
            placeId: place.place_id
          },
          function(place, status) {
            if (status === window.google.maps.places.PlacesServiceStatus.OK) {
              let marker = new window.google.maps.Marker({
                map: map,
                icon: icon,
                position: place.geometry.location,
                title: place.name,
                animation: window.google.maps.Animation.DROP
              });
              markers.push(marker);
              window.google.maps.event.addListener(marker, "click", function() {
                self.setState({
                  viewState: "mapDetailView",
                  placeData: place
                });
              });
            }
          }
        );

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
    this.setState({ map: map });
    map.setZoom(Math.min(map.getZoom(), 12));
  };

  getPlaceImages = place => {
    let images = "";
    place.photos.forEach(photo => {
      images += `<img src=${photo.getUrl({ maxHeight: 100 })}/>`;
    });
    return images;
  };

  geolocationHandler = () => {
    // Try HTML5 geolocation.
    let self = this;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function(position, showError) {
          let pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          if (!showError) {
            self.setState({
              viewState: "map",
              pos: { lat: pos.lat, lng: pos.lng }
            });
            return;
          }
        },
        function() {
          self.handleLocationError(true);
        }
      );
    } else {
      self.handleLocationError(false);
    }
  };

  handleLocationError = () => {
    this.setState({ viewState: "getLocation" });
  };

  setPosition = pos => {
    this.setState({ pos: pos });
  };

  setViewState = state => {
    window.ga("send", "event", {
      eventCategory: "Map View",
      eventAction: "click",
      eventLabel: state
    });

    switch (state) {
      case "map":
        this.setState({ viewState: "map" });
        break;
      case "getLocation":
        this.setState({ viewState: "getLocation" });
        break;
      case "mapDetailView":
        this.setState({ viewState: "mapDetailView" });
        break;
      case "init":
        this.setState({ viewState: "init" });
        break;
      default:
        this.setState({ viewState: "init" });
    }
  };

  render() {
    return (
      <div className="customMap">
        <button
          className="customMap__initMapBtn"
          onClick={this.geolocationHandler}
        >
          Try Out Custom Maps
        </button>
        <MapView
          viewState={this.state.viewState}
          mapPosition={this.state.pos}
          map={this.state.map}
        />
        <GetLocationView
          viewState={this.state.viewState}
          map={this.state.map}
          setViewState={this.setViewState}
          setPosition={this.setPosition}
        />
        <MapDetailView
          placeData={this.state.placeData}
          viewState={this.state.viewState}
          setViewState={this.setViewState}
        />
      </div>
    );
  }
}

export default CustomMap;
