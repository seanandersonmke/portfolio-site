import React, { Component } from "react";
import SinglePage from "./components/GlobalComponents";
import FilmSearch from "./components/FilmSearch";
import CustomMap from "./components/CustomMap";
import MainNav from "./components/MainNav";
import "./scss/index.scss";
import TmdbLogo from "./img/tmdblogo.svg";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initView: "init"
    };
  }

  initView = (view, type) => {
    this.setState({ initView: view });
    if (type) {
      this.handleOutboundLinkClicks(type);
    }
  };

  handleOutboundLinkClicks = type => {
    window.ga("send", "event", {
      eventCategory: "Nav Links",
      eventAction: "click",
      eventLabel: type
    });
  };

  render() {
    return (
      <main>
        <MainNav
          initView={this.initView}
          setIconView={this.state.iconView}
          setFilmView={this.state.initView}
        />
        <SinglePage
          id="homepage"
          icon="user"
          title="Sean Anderson"
          subtitle="Digital Creative"
          customContent="aboutme"
        />
        <SinglePage
          id="filmsearch"
          className="page"
          icon="film"
          title="Film Search"
          subtitle={
            <div className="filmSearch__logoContainer">
              <span>Basic film search demo made with the API from</span>
              <img src={TmdbLogo} alt="TMDB Logo" />{" "}
            </div>
          }
        >
          <FilmSearch
            initView={this.initView}
            setFilmView={this.state.initView}
          />
        </SinglePage>

        <SinglePage
          id="mapdemo"
          className="page"
          icon="film"
          title="Custom Map"
          subtitle="This is a highly customized Google Map. After searching for a place, click a marker to see a custom view using the Google Places API data."
        >
          <CustomMap initView={this.state.initView} />
        </SinglePage>

        <SinglePage
          id="browsersynth"
          className="page"
          icon="music"
          title="Browser Synth"
          subtitle="Synthesizers are cool (I have a few)"
          customContent="browsersynth"
        />

        <SinglePage
          id="spacestuff"
          className="page"
          icon="space-shuttle"
          title="More Coming Soon"
          subtitle="I am working on more fun demo tabs! In the meantime, enjoy this NASA Astronomy Pic of the Day!"
          customContent="spacestuff"
        />
      </main>
    );
  }
}

export default App;
